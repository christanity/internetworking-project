mkdir -p ~/sourcecode/snort_src/
cd ~/sourcesode/snort_src/
wget https://www.snort.org/downloads/snort/daq-2.0.6.tar.gz
tar xvfz daq-2.0.6.tar.gz
cd daq-2.0.6/
sudo apt-get install bison -y
sudo apt-get install flex -y
sudo apt-get install libpcap-dev -y
sudo apt-get install libpcre3-dev -y
sudo apt-get install libdumbnet-dev -y
sudo apt-get install checkinstall
./configure
make
sudo checkinstall -D --install=no --fstrans=no
sudo dpkg -i daq_2.0.6-1_armhf.deb
cd ~/sourcecode/snort_src/
wget https://snort.org/downloads/snort/snort-2.9.9.0.tar.gz
tar xvfz snort-2.9.9.0.tar.gz
cd ~/sourcecode/snort_src/snort-2.9.9.0
./configure --enable-sourcefire
make
sudo checkinstall -D --install=no --fstrans=no
sudo dpkg -i snort_2.9.9.0-1_armhf.deb
sudo ldconfig
which snort
sudo ln -s /usr/local/bin/snort /usr/sbin/snort
sudo groupadd snort
sudo useradd snort -r -s /sbin/nologin -c SNORT_IDS -g snort
sudo mkdir -p /etc/snort/rules/iplists
sudo mkdir /etc/snort/preproc_rules
sudo mkdir /usr/local/lib/snort_dynamicrules
sudo mkdir /etc/snort/so_rules
sudo touch /etc/snort/rules/local.rules
sudo touch /etc/snort/rules/iplists/white_list.rules
sudo touch /etc/snort/rules/iplists/black_list.rules
sudo mkdir /var/log/snort
sudo chmod -R 5775 /etc/snort
sudo chmod -R 5775 /var/log/snort
sudo chmod -R 5775 /usr/local/lib/snort_dynamicrules
sudo chown -R snort:snort /etc/snort
sudo chown -R snort:snort /var/log/snort
sudo chown -R snort:snort /usr/local/lib/snort_dynamicrules
cd ~/sourcecode/snort_src/snort-2.9.9.0/etc
sudo cp *.conf /etc/snort
sudo cp *.config /etc/snort
sudo cp *.map /etc/snort
cd ~/sourcecode/snort_src/snort-2.9.8.3/src/dynamic-preprocessors/build/usr/local/lib/snort_dynamicpreprocessor
sudo cp * /usr/local/lib/snort_dynamicpreprocessor
sudo sed -i �s/include \$RULE\_PATH/#include \$RULE\_PATH/� /etc/snort/snort.conf
