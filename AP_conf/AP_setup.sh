#!/bin/sh

apt-get update
apt-get -y install dnsmasq hostapd iptables

systemctl stop dnsmasq
systemctl stop hostapd

cp config/interfaces /etc/network/interfaces
cp config/dnsmasq.conf /etc/dnsmasq.conf
cp config/hostapd.conf /etc/hostapd/hostapd.conf
cp config/hostapd /etc/default/hostapd
#mkdir /opt/nat
#cp start_NAT.sh /opt/nat/start_NAT.sh
#cp config/nat.service /etc/systemd/system/multi-user.target.wants/nat.service

sudo rfkill unblock all
systemctl enable hostapd
systemctl start hostapd
systemctl enable dnsmasq
systemctl start dnsmasq
systemctl start nat
