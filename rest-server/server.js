const util = require('util');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const MongoClient = require('mongodb').MongoClient, assert = require('assert');
const mongourl = 'mongodb://localhost:27017/temp-sensor';
const PORT = 3000;

app.use(express.static('public'));
app.use(express.bodyParser());


// Rest endpoint to fetch all bookings from database
app.get('/temperatures', (request, response) => {
    MongoClient.connect(mongourl, function(err, db) {
	assert.equal(null, err);
	db.collection('temperatures').find().toArray(function(err, docs) {
	    assert.equal(null, err);
	    console.log(docs);
	    response.send(docs);
	});
	db.close();
    });
});

// Rest endpoint to insert bookings into database
app.post('/logtemp', (request, response) => {
    MongoClient.connect(mongourl, function(err, db) {
	    assert.equal(null, err);
	    db.collection('temperatures').insertOne(request.body,function(err, r) {
	    assert.equal(null, err);
	    assert.equal(1, r.insertedCount);
	});
	db.close();
    });
    response.send({this: 'happens'});
});

// Start express server on predefined port
app.listen(PORT, () => {
    console.log(`Running Express on port ${PORT}...`);
});
