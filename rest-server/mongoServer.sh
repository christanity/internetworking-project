#!/bin/sh

###IMPORTANT: Requires 'mongod.service' to be running via systemd"###

TERMINAL=gnome-terminal
#VAR1=$(locate /internetworking-project/rest-server | head -n 1)
#cd $VAR1 | 

$($TERMINAL -e "npm start")

SENSOR=$(locate sensor.py)

#Leave enough time for rest-server to start up before running python script
sleep 4s
$($TERMINAL -e "python $SENSOR")

#(OPTIONAL) Open up mongo command line  
#$"mongo"
