const { exec } = require('child_process');
const fs = require('fs');
const mongoose = require('mongoose');

var Change = require('../models/changes');
var configDB = require('../models/database');
var config = require('./config');

mongoose.connect(configDB.url, {
  useMongoClient: true,
});

function scp(){
  let cmdString = 'scp ' + config.client.user + '@' + config.client.ip + ':/home/' + config.client.user + '/internetworking-project/sensor.py ./temp'
  exec (cmdString, (error, stdout, stderr) => {
    if (error) {
      return console.log(error);
    }  
    readSave();
  });
}

function readSave() {
  var file = './temp/sensor.py'
  fs.readFile(file, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }
    console.log(data);
    var query = Change.findOne({name: 'sensor'}).sort('-date');
    query.exec(function (err, result) {
      if (err) {
        return console.log(err);
      }
      if ( result === null || data !== result.data) {
        var newDocument = new Change();
        newDocument.name = 'sensor';
        newDocument.date = Date();
        newDocument.data = data;
        newDocument.save(function(err) {
          if (err) {
            console.log(err);
          }
        });
      }
    });
  });
}

module.exports = {
  scp: scp,
}

