const fs = require('fs');
const { exec } = require('child_process');

var config = require('./config');

function scp(location, target, callback) {
  let cmdString = 'scp ' + location + ' ' + target
  exec (cmdString, (error, stdout, stderr) => {
    if (error) {
      return console.log(error);
    }
    callback();
  });
}

function getStatus(callback) {
  let cmdString = "ssh " + config.ap.user + '@' + config.ap.ip + " \"pgrep snort\""
  exec (cmdString, (error, stdout, stderr) => {
    let status = false;
    if (stdout !== "") {
      status = true;
    }
    callback(status);
  });
}

function command(command, callback) {
  let cmdString = "ssh " + config.ap.user + '@' + config.ap.ip;
  if (command === 'start') {
    cmdString += " \"sudo snort -A fast -u snort -g snort -c /etc/snort/snort.conf -l /var/log/snort -i "
    cmdString += config.ap.interface + '\"'
  } else {
    cmdString += " \"sudo pkill snort\""
  }
  console.log(cmdString);
  exec (cmdString, (error, stdout, stderr) => {
    callback('done');
  });
}

function getLogs(callback) {
  data = [];
  let cmdOne = "ssh " + config.ap.user + '@' + config.ap.ip + " \"ls /var/log/snort\""
  exec (cmdOne, (error, stdout, stderr) => {
    if (stdout.indexOf('alert') === -1) {
      callback(data);
    } else {
      let cmdTwo = "ssh " + config.ap.user + '@' + config.ap.ip + " \"cat /var/log/snort/alert\""
      exec (cmdTwo, (error, stdout, stderr) => {
        stdout = stdout.split('\n');
	let line = [];
	for (let i = 0; i < stdout.length; ++i) {
	  if (stdout[i] !== "") {
	    line = stdout[i].split(' [**] ');
	    line[1] = line[1].split('] ')[1];
	    data.push(line);
	  }
	}
	callback(data.reverse());
      });
    }
  });
}

function clearLogs(callback) {
  let cmdString = "ssh " + config.ap.user + '@' + config.ap.ip + " \"sudo rm /var/log/snort/alert\""
  console.log(cmdString);
  exec (cmdString, (error, stdout, stderr) => {
    callback(true);
  });
}

module.exports = {
  getStatus: getStatus,
  command: command,
  getLogs: getLogs,
  clearLogs: clearLogs,
}
