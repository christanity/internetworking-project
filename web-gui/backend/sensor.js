const axios = require('axios');

function getTemps(callback) {
  axios({
    method: 'get',
    url: 'http://chris-tan.xyz:3001/temperatures',
  })
  .then(function(response) {
    callback(response.data);
  })
  .catch((error) => {
    console.log(error);
  });
}

module.exports = {
  getTemps: getTemps,
}
