var ifconfig = require('ifconfig-linux')();
var nmap =  require('node-nmap');
var netmask = require('netmask').Netmask;

nmap.nmapLocation = "nmap";

function translateNetmask(mask) {
var netmask = {
    '0':0,
    '128':1,
    '192':2,
    '224':3,
    '240':4,
    '248':5,
    '252':6,
    '254':7,
    '255':8,
  }
  var slashMask = 0;

  var segments = mask.split(".");
  for (var x in segments) {
    slashMask += netmask[segments[x]];
  }
  return slashMask;
}

function ifConfig(callback) {
  ifconfig.then(function(result) {
    for (var key in result) {
      var slashMask = translateNetmask(result[key].inet.mask);
      result[key].inet['slashMask'] = slashMask;
      var block = new netmask(result[key].inet.addr + "/" + slashMask);
      result[key].inet['netAdd'] = block.base;
    }
//    console.log(result);
    netArray = []
    for (var key in result) {
      netArray.push(result[key]);
    }
    callback(netArray);
  }, function(err) {
    console.log(err);
    return(err);
  });
}

function scanNetwork(network, scanType, callback) {
  var scan = new nmap.NmapScan(network, "-" + scanType);
  scan.on('complete', function(data) {
    callback(data);
  });
  scan.on('error', function(error) {
    console.log(error);
  });
  scan.startScan();
}

module.exports = {
  ifconfig: ifConfig,
  scanNetwork: scanNetwork,
}
