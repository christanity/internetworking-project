const { exec } = require('child_process');

function runAttack(file, callback) {
  let attackString = 'msfconsole -r ./attacks/remote-login/' + file
  exec (attackString, (error, stdout, stderr) => {
    callback(error, stdout, stderr)
  });
}

module.exports = {
  runAttack: runAttack,
}
