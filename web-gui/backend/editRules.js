const fs = require('fs');
const { exec } = require('child_process');

const config = require('./config')

function scp(location, target, callback) {
  let cmdString = 'scp ' + location + ' ' + target
  exec (cmdString, (error, stdout, stderr) => {
    if (error) {
      return console.log(error);
    }
    callback();
  });
}

function getRules(callback) {
  let location = config.ap.user + '@' + config.ap.ip + ':/etc/snort/rules/local.rules'
  let target = './snort/config/local.rules'
  scp(location, target, () => {
    console.log(location);
    console.log(target);
    let rules = './snort/config/local.rules';
    fs.readFile(rules, 'utf8', (err, data) => {
      if (err) {
        return console.log(err);
      }
      let rulesList = data.split('\n');
      let finalList = []
      for ( let i = 0; i < rulesList.length; ++i) {
        if (rulesList[i] !== "") {
          finalList.push(rulesList[i])
        }
      }
      callback(finalList);
    });
  });
}

function deleteRule(rule, callback) {
  let ruleMsg
  if (rule.indexOf('\"') === -1) {
    ruleMsg = rule;
  } else {
    ruleMsg = rule.split('\"')[1].split('\"')[0]
  }
  console.log(ruleMsg);
  let cmdString = "sed -i '/" + ruleMsg + "/d' ./snort/config/local.rules"
  exec (cmdString, (error, stdout, stderr) => {
    if (error) {
      return console.log(error);
    }
    let location = './snort/config/local.rules'
    let target = config.ap.user + '@' + config.ap.ip + ':/etc/snort/rules/local.rules'
    scp(location, target, () => {
      getRules((data) => {
        callback(data);
      });
    });
  });
}

function checkRule(callback) {
  let cmdString = "ssh " + config.ap.user + '@' + config.ap.ip + " \"sudo snort -T -c /etc/snort/snort.conf -i ";
  cmdString += config.ap.interface + "\"";
  console.log(cmdString);
  exec (cmdString, (error, stdout, stderr) => {
    callback(stdout + '\n' + stderr)
  });
}

function addRule(rule, callback) {
  let cmdString = "echo '" + rule + "' >> ./snort/config/local.rules"
  exec (cmdString, (error, stdout, stderr) => {
    if (error) {
      return console.log(error);
    }
    let location = './snort/config/local.rules'
    let target = config.ap.user + '@' + config.ap.ip + ':/etc/snort/rules/local.rules'
    scp(location, target, () => {
      checkRule((data) => {
	console.log(data);
	console.log(data.indexOf('Snort successfully validated the configuration'));
	if (data.indexOf('Snort successfully validated the configuration') === -1) {
          deleteRule(rule, (data) => {
	    data.push('Error: Invalid Rule')
	    callback(data);
	  });
	} else {
	  getRules((data) => {
	    callback(data);
	  });
	}
      });
    });
  });
}

module.exports = {
  getRules: getRules,
  deleteRule: deleteRule,
  addRule: addRule,
}
