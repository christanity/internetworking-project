module.exports = {
  'ap': {
    'ip': '192.168.0.1',
    'user': 'pi',
    'interface': 'wlan0',
  },
  'client': {
    'ip': '192.168.0.18',
    'user': 'pi',
  },
  'server': {
    'ip': '172.104.190.137',
  },
  'attacker': {
    'ip': '192.168.0.4',
    'interface': 'enp0s3',
  },
  
}
