var axios = require('axios');
const mongoose = require('mongoose');
const configDB = require('../models/database.js');
var Changes = require('../models/changes');

mongoose.connect(configDB.url);

Changes.find({}, (err, changes) => {
  if (changes) {
    console.log(changes);
  } else {
    console.log(err);
  }
});
