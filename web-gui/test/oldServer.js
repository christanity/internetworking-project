var nmap = require('node-nmap');
var ifconfig = require('ifconfig-linux')();
var netmask = require('netmask').Netmask

nmap.nmapLocation = "nmap";


function translateNetmask(mask) {
  var netmask = {
    '0':0,
    '128':1,
    '192':2,
    '224':3,
    '240':4,
    '248':5,
    '252':6,
    '254':7,
    '255':8,
  }
  var slashMask = 0;  

  var segments = mask.split(".");
  for (var x in segments) {
    slashMask += netmask[segments[x]];
  }
  return slashMask;
}

function findNetworks(callBack) {
  ifconfig.then(function(result) {
    for (var key in result) {
      if (result[key].device !== 'lo') {
        var slashMask = translateNetmask(result[key].inet.mask);
        result[key].inet['slashMask'] = slashMask;
        var block = new netmask(result[key].inet.addr + "/" + slashMask)
        result[key].inet['netAdd'] = block.base;
      } 
    }
//    console.log(result);
    callBack(result);
  }, function(err) {
    return err;
  });
}

function scanNetworks(networks) {
  var scanResult = [];
  for (var key in networks) {
    if (networks[key].device !== 'lo') {
      var range = networks[key].inet.netAdd + "/"
      range += networks[key].inet.slashMask
      var scan = new nmap.NmapScan(range, '-sn')
      scan.on('complete', function(data) {
        console.log(data);
      });
      scan.on('error', function(error) {
        console.log(error);
      });
      scan.startScan();
    }
  }
}

findNetworks(scanNetworks);
