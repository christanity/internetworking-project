const fs = require('fs');
const mongoose = require('mongoose');

var Change = require('./models/changes');
var configDB = require('./models/database');

mongoose.connect(configDB.url);

var file = './temp/sensor.py';
fs.readFile(file, 'utf8', function (err, data) {
  if (err) {
    return console.log(err);
  }
  var newDocument = new Change();
  newDocument.name = 'sensor';
  newDocument.date = Date();
  newDocument.data = data;
  newDocument.save(function(err) {
    if (err) {
      console.log(err);
    }
    mongoose.connection.close();
  });
});
