var ifconfig = require('ifconfig-linux')();

function ifConfig(callback) {
  ifconfig.then(function(result) {
    console.log(result);
    callback(result);
  }, function(err) {
    console.log(err);
    return(err);
  });
}

//ifConfig();

module.exports = ifConfig;
