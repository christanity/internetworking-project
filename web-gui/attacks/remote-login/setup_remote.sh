mkdir /opt/metasploit-framework/sensor-version
cp sensor.py /opt/metasploit-framework/sensor-version/
cp add10.py /opt/metasploit-framework/sensor-version/
cp minus10.py /opt/metasploit-framework/sensor-version/
cp edit-sensor.rb /opt/metasploit-framework/embedded/framework/scripts/meterpreter
cp edit-add10.rb /opt/metasploit-framework/embedded/framework/scripts/meterpreter
cp edit-minus10.rb /opt/metasploit-framework/embedded/framework/scripts/meterpreter
