const mongoose = require('mongoose');

var Change = require('./models/changes');
var configDB = require('./models/database');

mongoose.connect(configDB.url);

var query = Change.findOne({name: 'sensor'}).sort('-date');

query.exec(function (err, result) {
  if (err) {
    return console.log(err);
  }
  console.log(result.data);
});

mongoose.connection.close();
