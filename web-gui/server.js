const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cron = require('node-cron');
const mongoose = require('mongoose');

const netmap = require('./backend/netmap');
const check = require('./backend/changeCheck');
const remoteLogin = require('./backend/remoteLogin');
const editRules = require('./backend/editRules');
const snort = require('./backend/snort');
const mitm = require('./backend/mitm');
const sensor = require('./backend/sensor');

const configDB = require('./models/database');
const config = require('./backend/config');
var Changes = require('./models/changes');

const PORT = 3000;

mongoose.connect(configDB.url, {
  useMongoClient: true,
});

app.use(express.static('dist'));
app.use(express.bodyParser());

app.get('/getSensorTemps', (request, response) => {
  sensor.getTemps((data) => {
    response.send(data);
  });
});

app.get('/sensorCommand', (request, response) => {
  console.log(request.query.command);
  mitm.commandSensor(request.query.command, (data) => {
    response.send(data);
  });
});

app.get('/sensorStatus', (request, response) => {
  mitm.sensorStatus((data) => {
    console.log(data);
    response.send(data);
  });
});

app.get('/arpSpoofCommand', (request, response) => {
  console.log(request.query.command);
  mitm.commandArpSpoof(request.query.command, (data) => {
    response.send(data);
  });
});

app.get('/arpSpoofStatus', (request, response) => {
  mitm.arpSpoofStatus((data) => {
    response.send(data);
  });
});

app.get('/tcpDumpCommand', (request, response) => {
  console.log(request.query.command);
  mitm.commandTcpDump(request.query.command, (data) => {
    response.send(data);
  });
});

app.get('/tcpDumpStatus', (request, response) => {
  mitm.tcpDumpStatus((data) => {
    response.send(data);
  });
});

app.get('/clearSnortLogs', (request, response) => {
  snort.clearLogs((data) => {
    response.send(data);
  });
});

app.get('/snortAlerts', (request, response) => {
  snort.getLogs((data) => {
    response.send(data);
  });
});

app.get('/snortCommand', (request, response) => {
  console.log(request.query.command);
  snort.command(request.query.command, (data) => {
    response.send(data);
  });
});

app.get('/snortStatus', (request, response) => {
  snort.getStatus((data) => {
    response.send(data);
  });
});

app.get('/getHosts', (request, response) => {
  response.send(config);
});

app.post('/addRule', (request, response) => {
  editRules.addRule(request.body.rule, (data) => {
    response.send(data);
  });
});

app.get('/deleteRule', (request, response) => {
  editRules.deleteRule(request.query.rule, (data) => {
    response.send(data);
  });
});

app.get('/snortRules', (request, response) => {
  editRules.getRules((data) => {
    response.send(data);
  });
});

app.get('/remoteLogin', (request, response) => {
  console.log(request.query.file)
  remoteLogin.runAttack(request.query.file, function(error, stdout, stderr) {
    let data = {
      'error': error,
      'stdout': stdout,
      'stderr': stderr
    };
    response.send(data);
  });
});

app.get('/changes', (request, response) => {
  Changes.find({}, (err, changes) => {
    if (changes) {
      response.send(changes);
    } else {
      console.log(err);
    }
  });
});

app.get('/ifconfig', (request, response) => {
  netmap.ifconfig(function (data) {
    response.send(data);
  });
});

app.get('/nmap', (request, response) => {
  console.log(request.query);
  netmap.scanNetwork(request.query.network, request.query.scan, function(data) {
    response.send(data);
  });
});

// Start express server on predefined port
app.listen(PORT, () => {
  console.log(`Running Express on port ${PORT}...`);
});

var watch = cron.schedule('0,10,20,30,40,50 * * * * *', function() {
  check.scp();
  console.log('check');
});
watch.start();

