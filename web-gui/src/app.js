import React from 'react';
import SideBar from './modules/components/sidebar';
import Main from './main';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <SideBar />
	<Main />
      </div>
    );
  }
}

/*
const App = () => (
  <div>
    <SideBar />
    <Main />
  </div>
)

export default App*/
