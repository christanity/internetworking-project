import React from 'react';
import { Switch, Route } from 'react-router-dom';
import NetworkMap from './modules/network_map';
import ScanHost from './modules/scan_host';
import MitM from './modules/mitm';
import RemoteLogin from './modules/remote_login';
import SnortLogs from './modules/snort_logs';
import SnortConfig from './modules/snort_config';
import ShowChanges from './modules/show_changes';
import SensorTemps from './modules/sensor_temps';

const Main = () => (
  <div id="mainWindow">
    <Switch>
      <Route exact path ='/' component={NetworkMap} /> 	
      <Route exact path ='/networkMap' component={NetworkMap} /> 	
      <Route exact path ='/scanHost' component={ScanHost} /> 	
      <Route exact path ='/manInMiddle' component={MitM} /> 	
      <Route exact path ='/remoteLogin' component={RemoteLogin} /> 	
      <Route exact path ='/snort' component={SnortLogs} /> 	
      <Route exact path ='/snortConfig' component={SnortConfig} /> 	
      <Route exact path ='/showChanges' component={ShowChanges} /> 	
      <Route exact path ='/sensorTemps' component={SensorTemps} /> 	
    </Switch>
  </div>
)

export default Main
