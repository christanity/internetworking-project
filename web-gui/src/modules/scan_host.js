import React from 'react';
import axios from 'axios';
import ScanWindow from './components/scan_window';

export default class ScanHost extends React.Component {
  constructor() {
    super();
    this.state = {
      hosts: [],
      display: null,
      ip: null,
      scanning: null,
      hostsScanned: false,
      refreshString: 'Refresh'
    };
  }

  getHosts() {
    axios.get('/ifconfig')
    .then((response) => {
      console.log(response);
      for ( let i = 0; i < response.data.length; ++i) {
	console.log(response.data[i].device)
        if (response.data[i].device === "enp0s3") {
	  let network = response.data[i].inet.netAdd + '/' + response.data[i].inet.slashMask;
	  let nmapString = 'network=' + network + '&scan=sn'
	  console.log(nmapString);
	  axios.get('/nmap?' + nmapString)
	  .then ((nmapRes) => {
	    this.setState({
	      hosts: nmapRes.data,
	      hostsScanned: true,
	      refreshString: 'Refresh'
	    });
	    console.log(this.state.hosts);
	  })
	  .catch((error) => {
	    console.log(error);
	  });
	}
      }
    })
    .catch((error) => {
      console.log(error);
    });
  }

  scanHost(address) {
    this.setState({
      scanning: true,
      ip: address
    });
    let nmapString = '?network=' + address + '&scan=O';
      console.log(nmapString);
      axios.get('/nmap' + nmapString)
      .then ((response) => {
        this.setState({
	  display: response.data,
	  scanning: false
	});
     })
      .catch((error) => {
        this.setState({
	  display: []
	});
      });
  }

  refreshHosts() {
    this.setState({
      refreshString: 'Refreshing...'
    });
    this.getHosts();
  }

  componentWillMount() {
    this.getHosts();
  }

  render(){
    let refreshStatus = 'Refresh'
    if (this.state.hosts.refreshing) {
      refreshStatus = 'Refreshing...'
    }
    return (
      <div>
        <div id="changeBar" className="w3-sidebar w3-grey w3-bar-block">
	  <button className="w3-bar-item w3-button w3-light-grey" onClick={this.refreshHosts.bind(this)}>{this.state.refreshString}</button>
	    {this.state.hosts.map((host, key) => 
	      <button onClick={this.scanHost.bind(this, host.ip)} className="w3-bar-item w3-button w3-border-black" key={key}>
                {host.hostname} {host.ip}
	      </button>
	    )}
	</div>
	<ScanWindow display={this.state.display} ip={this.state.ip} scanning={this.state.scanning} hostsScanned={this.state.hostsScanned} />
      </div>
    );
  }
}
