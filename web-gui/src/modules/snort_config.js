import React from 'react';
import axios from 'axios';

export default class SnortConfig extends React.Component {
  constructor() {
    super();
    this.state = {
      rules: [],
      errorMsg: "",
    };
  }

  getRules() {
    axios.get('/snortRules')
    .then((response) => {
      this.setState({
        rules: response.data
      });
    })
    .catch((error) => {
      console.log(error);
    });
  }

  deleteRule(rule) {
    axios.get('/deleteRule?rule=' + rule)
    .then((response) => {
      this.setState({
        rules: response.data
      });
    })
    .catch((error) => {
      console.log(error);
    });
  }

  addRule(event) {
    event.preventDefault();
    this.setState({
      errorMsg: 'Verifying Rule...'
    });
    axios.post('/addRule', {
      rule: this.rule.value,
    })
    .then((response) => {
      if (response.data[response.data.length - 1] === 'Error: Invalid Rule') {
	this.setState({
	  errorMsg: response.data[response.data.length - 1]
	});
	response.data.splice(response.data.length - 1, 1);
      } else {
        this.setState({
	  errorMsg: ""
	});
      }
      this.setState({
        rules: response.data
      });
      this.rule.value = "";
    })
    .catch((error) => {
      console.log(error);
    });
  }

  componentWillMount() {
    this.getRules();
  }

  render(){
    return (
      <div>
        <div className="w3-container w3-indigo">
	  <h1>Snort Rules</h1>
	  <h5>View, delete and add snort rules</h5>
	</div>
	<table className="w3-table w3-bordered w3-border">
	  <tbody>
	    {this.state.rules.map((rule, key) =>
	      <tr key={key}>
	        <td>{rule}</td>
		<td>
		  <button onClick={this.deleteRule.bind(this, rule)} >
		    Delete
		  </button>
		</td>
	      </tr>
            )}
	  </tbody>
	</table>
	<form className="w3-container w3-card-4 w3-margin" onSubmit={this.addRule.bind(this)}>
	  <p>      
	    <label className="w3-text-indigo"><b>Add a Snort Rule</b></label>
	    <textarea className="w3-input w3-border" ref={rule => this.rule = rule} type="text" /> 
	  </p>
	  <p><button type="submit" className="w3-btn w3-indigo">Add Rule</button></p>
	</form>
	<span id="alert-msg">{this.state.errorMsg}</span>
      </div>
    );
  }
}
