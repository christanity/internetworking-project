import React from 'react';
import axios from 'axios';

export default class NetworkMap extends React.Component {
  constructor() {
    super();
    this.state = {
      loaded: false,
      hosts: {
        'ap': {
	  'ip': 'Loading',
	  'user': 'Loading...',
	  'status': {
	    'string': 'Loading Status...',
	    'color': 'w3-yellow',
	  },
        },
        'client': {
          'ip': 'Loading...',
	  'user': 'Loading...',
	  'status': {
	    'string': 'Loading Status...',
	    'color': 'w3-yellow',
	  },
        },
        'server': {
          'ip': 'Loading...',
	  'status': {
	    'string': 'Loading Status...',
	    'color': 'w3-yellow',
	  },
        },
        'attacker': {
          'ip': 'Loading...',
	  'status': {
	    'string': 'Loading Status...',
	    'color': 'w3-yellow',
	  },
        },
      }
    };
  }
  
  checkHost(host, ip) {
    axios.get('/nmap?network=' + ip +'&scan=sn')
    .then((response) => {
      if (response.data.length > 0) {
	this.state.hosts[host].status.color = 'w3-green'
	this.state.hosts[host].status.string = 'Online'
      } else {
	this.state.hosts[host].status.color = 'w3-red'
	this.state.hosts[host].status.string = 'Offline'
      }
      this.forceUpdate();
    })
    .catch((error) => {
      console.log(error);
    });
  }

  getHosts() {
    axios.get('/getHosts')
    .then((response) => {
      for (let key in response.data) {
        if (response.data.hasOwnProperty(key)) {
	  response.data[key]['status'] = {
	    color: 'w3-yellow',
	    string: 'Loading Status...'
	  };
	}
      }
      this.setState({
	loaded: true,
        hosts: response.data
      });
      this.checkAllHosts();
    })
    .catch((error) => {
      console.log(error);
    });
  }

  checkAllHosts() {
    if (this.state.loaded) {
      this.checkHost('server', this.state.hosts.server.ip);
      this.checkHost('ap', this.state.hosts.ap.ip);
      this.checkHost('client', this.state.hosts.client.ip);
      this.checkHost('attacker', this.state.hosts.attacker.ip);
    }
  }

 componentWillMount() {
    this.getHosts();
  }

  componentDidMount() {
    this.interval = setInterval(this.checkAllHosts.bind(this), 15000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div>
	<div className="w3-container w3-indigo">
	  <h1>Network Map</h1>
	  <h5>Shows the topology of the network and displays the role, IP address and status of each device</h5>
	</div>
	<div>
	  <table>
	    <tbody>
	      <tr>
	        <td></td>
	        <td>
	          <div className="map-card w3-card-4">
	            <header className="w3-container w3-teal">
	              <h2>Server</h2>
	              <h5>IP Address: {this.state.hosts.server.ip}</h5>
	            </header>
                    <div className="w3-container">
	              <p>This server receives the temperature sensor data that is being collected on the client via the Sense HAT and saves it to a database.</p>
	            </div>
	            <footer className={'w3-container ' + this.state.hosts.server.status.color}>
	              <h4>{this.state.hosts.server.status.string}</h4>
	            </footer>
	          </div>
	        </td>
	        <td></td>
	      </tr>
	      <tr>
	        <td></td>
	        <td>
	          <svg className="map-card">
	            <line x1="200" y1="0" x2="200" y2="150" stroke="black" strokeWidth="2"/>
	          </svg>
	        </td>
	        <td></td>
	      </tr>
	      <tr>
	        <td></td>
	        <td>
	          <div className="map-card w3-card-4">
	            <header className="w3-container w3-teal">
	              <h2>Access Point</h2>
	              <h5>IP Address: {this.state.hosts.ap.ip}</h5>
	            </header>
                    <div className="w3-container">
	              <p>The WiFi access point provides network/internet access to the client and attacker via NAT. It also hosts the Snort IDS instance.</p>
	            </div>
	            <footer className={'w3-container ' + this.state.hosts.ap.status.color}>
	              <h4>{this.state.hosts.ap.status.string}</h4>
	            </footer>
	          </div>
	        </td>
	        <td></td>
	      </tr>
	      <tr>
	        <td>
	          <svg className="map-card">
	            <line x1="200" y1="150" x2="400" y2="0" stroke="black" strokeWidth="2"/>
	          </svg>
	        </td>
	        <td></td>
	        <td>
	          <svg className="map-card">
	            <line x1="200" y1="150" x2="0" y2="0" stroke="black" strokeWidth="2"/>
	          </svg>
	        </td>
	      </tr>
	      <tr>
	        <td>
	          <div className="map-card w3-card-4">
	            <header className="w3-container w3-teal">
	              <h2>Client / Temperature Sensor</h2>
	              <h5>IP Address: {this.state.hosts.client.ip}</h5>
	            </header>
                    <div className="w3-container">
	              <p>The sensor uses a Sense HAT to make regular temperature readings and send the data to the server.</p>
	            </div>
	            <footer className={'w3-container ' + this.state.hosts.client.status.color}>
	              <h4>{this.state.hosts.client.status.string}</h4>
	            </footer>
	          </div>
	        </td>
	        <td></td>
	        <td>
	          <div className="map-card w3-card-4">
	            <header className="w3-container w3-teal">
	              <h2>Attacker / Web-GUI</h2>
	              <h5>IP Address: {this.state.hosts.attacker.ip}</h5>
	            </header>
                    <div className="w3-container">
	              <p>The attacker carries network reconnaissance and attacks and also hosts this Web Interface to carry execute them.</p>
	            </div>
	            <footer className={'w3-container ' + this.state.hosts.attacker.status.color}>
	              <h4>{this.state.hosts.attacker.status.string}</h4>
	            </footer>
	          </div>
	        </td>
	      </tr>
	    </tbody>
	  </table>
	</div>
      </div>
    );
  }
}
