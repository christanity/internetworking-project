import React from 'react';
import axios from 'axios';

export default class SnortLogs extends React.Component {
  constructor() {
    super();
    this.state = {
      logs: [['Loading logs...','','']],
      running: null,
      statusString: 'Loading Snort status...',
      statusColor: 'w3-yellow',
      statusButton: 'Loading...'
    }
  }

  handleClick() {
    this.setState({
      statusButton: 'Loading...',
      statusColor: 'w3-yellow',
      statusString: 'Loading Snort status...',
    });
    setTimeout(() => {
      this.snortStatus();
    }, 3500);
    let command = 'start'
    if (this.state.running) {
      command = 'stop'
    }
    axios.get('/snortCommand?command=' + command)
    .then((response) => {
      setTimeout(() => {
        this.snortStatus();
      },1500);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  snortStatus() {
    axios.get('/snortStatus')
    .then((response) => {
      if (response.data) {
        this.setState({
	  statusString: 'Snort is running...',
	  statusColor: 'w3-green',
	  statusButton: 'Stop Snort',
	  running: true,
	});
      } else {
        this.setState({
	  statusString: 'Snort is stopped',
	  statusColor: 'w3-red',
	  statusButton: 'Start Snort',
	  running: false,
	});
      }
    })
    .catch((error) => {
      console.log(error);
    });
  }

  getLogs() {
    axios.get('/snortAlerts')
    .then((response) => {
      if (response.data.length < 1) {
        this.setState({
	  logs: [['No Logs available','','']]
	});
      } else {
        this.setState({
          logs: response.data
        });
      }
    })
    .catch((error) => {
      console.log(error);
    });
  }

  handleClear() {
    axios.get('/clearSnortLogs')
    .then ((response) => {
      this.getLogs();
    })
    .catch((error) => {
      console.log(error);
    });
  }

  componentWillMount() {
    this.snortStatus();
    this.getLogs();
  }

  componentDidMount() {
    this.interval = setInterval(this.getLogs.bind(this), 10000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render(){
    return (
      <div>
        <div className="w3-container w3-indigo">
	  <h1>Snort IDS</h1>
	  <h5>Shows the status and alert log of Snort as well as the ability to start / stop Snort.</h5>
	</div>
	<div className={'w3-container ' + this.state.statusColor}>
	  <span className="snort-status"><h3>{this.state.statusString}</h3></span>
	  <span className="snort-padding" />
	  <button onClick={this.handleClick.bind(this)} className="w3-button w3-white w3-border w3-border-black w3-round-large snort-status">{this.state.statusButton}</button>
	  <span className="snort-padding" />
	  <button onClick={this.handleClear.bind(this)} className="w3-button w3-white w3-border w3-border-black w3-round-large snort-status">Clear Logs</button>
	</div>
	<div>
	  <table className="w3-table-all">
	    <thead>
	      <tr className="w3-light-grey">
	        <th>Time of Log</th>
	        <th>Alert Msg</th>
	        <th>Type of Traffic</th>
	      </tr>
	    </thead>
	    <tbody>
	      {this.state.logs.map((log, key) => 
	        <tr key={key}>
		  <td>{log[0]}</td>
		  <td>{log[1]}</td>
		  <td>{log[2]}</td>
		</tr>
	      )}
	    </tbody>
	  </table>    
	</div>
      </div>
    );
  }
}
