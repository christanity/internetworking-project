import React from 'react';
import axios from 'axios';
import Diff from './react_diff';

export default class ChangeWindow extends React.Component {
	
  render() {
    let changeDate = this.props.value.date;
    if (this.props.value.date !== undefined) {
      changeDate =  this.props.value.date.split("T")[0] + ' ';
      changeDate += this.props.value.date.split("T")[1].split(".")[0];
    }

    return (
      <div>
	<div id="changeHeader" className="w3-container w3-indigo">
	  <h1>Changes Detected</h1>
	  <h5>Constantly polls the sensor.py file on the client and makes a record if it is different from the last saved version</h5>
	</div>
	<div id="changeWindow">
	  <table className="w3-table-all">
	    <thead>
              <tr className="w3-light-grey">
		<th>
	          Edited on: {changeDate}
	        </th>
	        <th>Old Version</th>
	      </tr>
	    </thead>
	    <tbody>
              <tr className="codeFormat">
	        <td><Diff inputA={this.props.value.past} inputB={this.props.value.data} type="lines" /></td>
	        <td>{this.props.value.past}</td>
	      </tr>
	    </tbody>
	  </table>
	</div>
      </div>
    );
  }
}
