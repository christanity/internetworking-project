import React from 'react';
import axios from 'axios';

export default class ScanWindow extends React.Component {
  render() {
    return (
      <div>
        <div id="changeHeader" className="w3-container w3-indigo">
	  <h1>Scan Host</h1>
	  <h5>Uses Nmap with the -O flag to detect operating system and open ports</h5>
	</div>
	<div id="changeWindow" className="codeFormat">
	  {(() => {if (!this.props.hostsScanned && this.props.scanning === null) {
	      return 'Scanning for hosts...';
	    } else if (this.props.hostsScanned && this.props.scanning === null) {
	      return 'Please select a host to scan for more details'
	    }else if (this.props.scanning === true) {
	      return 'Scanning ' + this.props.ip;
	    } else if (this.props.scanning === false && this.props.display.length > 0) {
	      console.log(this.props.display);
	      return (
	        <table className="w3-table-all">
		  <thead>
		    <tr className="w3-light-grey">
		      <th>Hostname:</th>
		      <th>{this.props.display[0].hostname}</th>
		    </tr>
		  </thead>
		  <tbody>
                    <tr>
		      <td>IP Address:</td>
		      <td>{this.props.display[0].ip}</td>
		    </tr>    
                    <tr>
		      <td>Mac Address:</td>
		      <td>{this.props.display[0].mac}</td>
		    </tr>    
	            <tr>
		      <td>Operating System:</td>
		      <td>{this.props.display[0].osNmap}</td>
		    </tr>    
	            <tr>
		      <td>Vendor:</td>
		      <td>{this.props.display[0].vendor}</td>
		    </tr>    
                    <tr>
		      <td>Open Ports:</td>
		      <td><ul className="port-list">{
		        this.props.display[0].openPorts.map((openPort, key) => 
			  <li key={key}>
			    <table className="w3-table w3-bordered">
			      <thead>
			        <tr className="w3-light-grey">
				  <th className="port-label">Service:</th>
				  <th>{openPort.service}</th>
				</tr>
			      </thead>
			      <tbody>
				<tr>
				  <td className="port-label">Protocol:</td>
				  <td>{openPort.protocol}</td>
				</tr>
				<tr className="w3-white">
				  <td className="port-label">Port:</td>
				  <td>{openPort.port}</td>
				</tr>
			      </tbody>
			    </table>
			  </li>
			) 
		      }</ul></td>
		    </tr>    
		  </tbody>
		</table>
	      );
	    } else if (this.props.display.length === 0) {
	      return 'Error: not able to scan host';
	    }
	  })()}    
	</div>
      </div>
    );
  }
}
