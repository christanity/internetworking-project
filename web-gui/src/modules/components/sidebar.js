import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

export default class SideBar extends React.Component {
  render(){
    return (
      <div id="sidebar" className="w3-sidebar w3-black w3-bar-block">
	<h3 className="w3-bar-item" >Internetworking Project</h3>
	<Link to='/networkMap' className="w3-bar-item w3-button">Network Map</Link>
	<Link to='/scanHost' className="w3-bar-item w3-button">Scan Host</Link>
        <Link to='/manInMiddle' className="w3-bar-item w3-button">Man in the Middle</Link>
        <Link to='/remoteLogin' className="w3-bar-item w3-button">Remote Login</Link>
        <Link to='/snort' className="w3-bar-item w3-button">Snort IDS</Link>
        <Link to='/snortConfig' className="w3-bar-item w3-button">Snort Config</Link>
        <Link to='/showChanges' className="w3-bar-item w3-button">Show Changes</Link>
        <Link to='/sensorTemps' className="w3-bar-item w3-button">Temperature Records</Link>
      </div>
    );
  }
}
