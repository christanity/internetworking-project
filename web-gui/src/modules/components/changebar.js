import React from 'react';
import axios from 'axios';
import ChangeWindow from './change_window';

export default class ChangeBar extends React.Component {
  constructor() {
    super();
    this.state = {
      changes: [],
      display: 1,
      refreshString: 'Refresh'
    };
  }
  getChanges() {
    axios.get('/changes')
    .then((response) => {
      let changeHistory = [];
      let current;
      let count = 1;
      for ( let i = response.data.length - 1; i > 0; --i) {
        current = response.data[i];
	current['num'] = count;
	current['past'] = response.data[i-1].data;
	changeHistory.push(current);
	if (count === 1) {
	  this.setState({
	    display: current
	  });
	}
	++count;
      }
      this.setState({
        changes: changeHistory,
	refreshString: 'Refresh'
      });
    })
    .catch((error) => {
      console.log(error);
    });
  }


  updateChanges() {
    axios.get('/changes')
    .then((response) => {
      let changeHistory = [];
      let current;
      let count = 1;
      for ( let i = response.data.length - 1; i > 0; --i) {
        current = response.data[i];
	current['num'] = count;
	current['past'] = response.data[i-1];
	changeHistory.push(current);
	++ count;
      }
      this.setState({
        changes: changeHistory
      });
    })
    .catch((error) => {
      console.log(error);
    });
  }

  setDisplay(num) {
    for (let i = 0; i < this.state.changes.length; ++i) {
      if (this.state.changes[i].num === num) {
        this.setState({
	  display: this.state.changes[i]
	});
      }
    }
  }

  refreshChanges() {
    this.setState({
      refreshString: 'Refreshing...'
    });
    this.getChanges();
  }

  componentWillMount() {
    this.getChanges();
  }

  render() {
    return (
      <div>
        <div id="changeBar" className="w3-sidebar w3-grey w3-bar-block">
	  <button className="w3-bar-item w3-button w3-light-grey " onClick={this.refreshChanges.bind(this)}>{this.state.refreshString}</button>
	  {this.state.changes.map((change, key) => 
	    <button onClick={this.setDisplay.bind(this, change.num)} className="w3-bar-item w3-button" key={key}>
	      {change.date.split("T")[0]} {change.date.split("T")[1].split(".")[0]}
	    </button>
	  )}
        </div>
	<ChangeWindow value={this.state.display} />
      </div>
    );
  }
}

