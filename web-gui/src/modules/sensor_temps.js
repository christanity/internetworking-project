import React from 'react';
import axios from 'axios';

export default class SensorTemps extends React.Component {
  constructor() {
    super();
    this.state = {
      temps: [],
      msg: 'Loading temperature records...'
    };
  }

  getTemps() {
    axios.get('/getSensorTemps')
    .then((response) => {
      console.log(response);
      this.setState({
        temps: response.data,
        msg: "",
      });
    })
    .catch((error) => {
      console.log(error);
    });
  }

  componentWillMount() {
    this.getTemps();
  }

  componentDidMount() {
    this.interval = setInterval(this.getTemps.bind(this), 5000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div>
        <div className="w3-container w3-indigo">
	  <h1>Temperature Records</h1>
	  <h5>Records logged by temperature sensor</h5>
	</div>
	<table className="w3-table w3-bordered w3-border">
	  <tbody>
	  {this.state.temps.map((temp, key) =>
	    <tr key={key}>
	      <td>{temp.logged_at}</td>
	      <td>{temp.temperature}</td>
	    </tr>
	  )}
	  </tbody>
	</table>
      </div>
    )
  }
}
