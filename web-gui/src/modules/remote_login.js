import React from 'react';
import axios from 'axios';

export default class RemoteLogin extends React.Component {
  constructor() {
    super();
    this.state = {
      message: ''
    }
  }

  runAttack(file) {
    this.setState({
      message: 'Running metasploit reverse tcp attack...'
    });
    setTimeout(()=> {
      this.setState({
        message: 'Attack complete!'
      });
    }, 20000);
    axios.get('/remoteLogin?file=' + file)
    .then((response) => {
      console.log(response.data)
      if (response.data.error) {
        this.setState({
	  message: reponse.data.stderr
	});
      } else {
        this.setState({
	  message: response.data.stdout
	});
      }
    })
    .catch((error) => {
      console.log(error);
    });
  }
	
  render(){
    return (
      <div>
        <div className="w3-container w3-indigo">
	  <h1>Remote Login</h1>
	  <h5>Uses metasploit to get remote root access to client via SMB vulnerability and then overwrites sensor.py</h5>
	</div>
	<div>
	  Please choose a version of sensor.py to upload via metasploit attack:
	  <table className="w3-table w3-border w3-bordered">
	    <tbody>
	      <tr>
	        <td>Original file with no alterations</td>
	        <td><button onClick={this.runAttack.bind(this, 'meta_sensor.rc')}>Run Attack</button></td>
	      </tr>
	      <tr>
	        <td>Adds 10 degrees celsius to the temperature sensor reading</td>
	        <td><button onClick={this.runAttack.bind(this, 'meta_add10.rc')}>Run Attack</button></td>
	      </tr>
	      <tr>
	        <td>Minuses 10 degrees celsius from the temperature sensor reading</td>
	        <td><button onClick={this.runAttack.bind(this, 'meta_minus10.rc')}>Run Attack</button></td>
	      </tr>
	    </tbody>
	  </table>
	</div>
	<span id="alert-msg">{this.state.message}</span>
      </div>
    );
  }
}
