import React from 'react';
import axios from 'axios';
import { Switch, Route } from 'react-router-dom'
import ChangeHome from './components/change_home';

export default class ShowChanges extends React.Component {
  render(){
    return (
      <Switch>
        <Route exact path='/showChanges' component={ChangeHome} />
      </Switch>
    );
  }
}
