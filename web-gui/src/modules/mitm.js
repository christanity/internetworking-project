import React from 'react';
import axios from 'axios';

export default class MitM extends React.Component {
  constructor() {
    super();
    this.state = {
      dump: [],
      sensor: {
        running: null,
	string: 'Loading Sensor status...',
	color: 'w3-yellow',
	button: 'Loading...'
      },
      arpSpoof: {
        running: null,
	string: 'Loading Arpspoof status...',
	color: 'w3-yellow',
	button: 'Loading...'
      },
      tcpDump: {
        running: null,
	string: 'Loading Tcpdump status...',
	color: 'w3-yellow',
	button: 'Loading...'
      },

    }
  }

  sensorStatus() {
    axios.get('/sensorStatus')
    .then((response) => {
      if (response.data) {
        this.setState({
	  sensor: {
            string: 'Sensor is running...',
            color: 'w3-green',
	    button: 'Stop Sensor',
	    running: true,
	  }
        });
      } else {
        this.setState({
	  sensor: {	
	    string: 'Sensor is stopped',
	    color: 'w3-red',
	    button: 'Start Sensor',
	    running: false,
          }
        });
      }
    })
    .catch((error) => {
      console.log(error);
    });
  }

  arpSpoofStatus() {
    axios.get('/arpSpoofStatus')
    .then((response) => {
      if (response.data) {
        this.setState({
	  arpSpoof: {
            string: 'Arpspoof is running...',
            color: 'w3-green',
	    button: 'Stop Arpspoof',
	    running: true,
	  }
        });
      } else {
        this.setState({
	  arpSpoof: {	
	    string: 'Arpspoof is stopped',
	    color: 'w3-red',
	    button: 'Start Arpspoof',
	    running: false,
          }
        });
      }
    })
    .catch((error) => {
      console.log(error);
    });
  }

  tcpDumpStatus() {
    axios.get('/tcpDumpStatus')
    .then((response) => {
      if (response.data) {
        this.setState({
	  tcpDump: {
            string: 'Tcpdump is running...',
            color: 'w3-green',
	    button: 'Stop Tcpdump',
	    running: true,
	  }
        });
      } else {
        this.setState({
	  tcpDump: {	
	    string: 'Tcpdump is stopped',
	    color: 'w3-red',
	    button: 'Start Tcpdump',
	    running: false,
          }
        });
      }
    })
    .catch((error) => {
      console.log(error);
    });
  }

  handleSensor() {
    this.setState({
      sensor: {
	button:'Loading...',
	string: 'Loading Sensor status...',
	color: 'w3-yellow',
      },
    });
    setTimeout(() => {
      this.sensorStatus();
    }, 3500);
    let command = 'start'
    if (this.state.sensor.running) {
      command = 'stop'
    }
    axios.get('/sensorCommand?command=' + command)
    .then((response) => {
      this.sensorStatus();
    })
    .catch((error) => {
      console.log(error);
    });
  }

  handleArpSpoof() {
    this.setState({
      arpSpoof: {
	button:'Loading...',
	string: 'Loading Arpspoof status...',
	color: 'w3-yellow',
      },
    });
    setTimeout(() => {
      this.arpSpoofStatus();
    }, 3500);
    let command = 'start'
    if (this.state.arpSpoof.running) {
      command = 'stop'
    }
    axios.get('/arpSpoofCommand?command=' + command)
    .then((response) => {
      setTimeout(() => {
        this.arpSpoofStatus();
      }, 1000);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  handleTcpDump() {
    this.setState({
      tcpDump: {
	button:'Loading...',
	string: 'Loading Tcpdump status...',
	color: 'w3-yellow',
      },
    });
    setTimeout(() => {
      this.tcpDumpStatus();
    }, 3500);
    let command = 'start'
    if (this.state.tcpDump.running) {
      command = 'stop'
    }
    axios.get('/tcpDumpCommand?command=' + command)
    .then((response) => {
      this.tcpDumpStatus();
    })
    .catch((error) => {
      console.log(error);
    });
  }  

  componentWillMount() {
    this.sensorStatus();
    this.arpSpoofStatus();
    this.tcpDumpStatus();
  }

  render(){
    return (
      <div>
        <div className="w3-container w3-indigo">
          <h1>Man in the Middle</h1>
          <h5>This attack uses arpspoof to reroute the sensor packets to the access point via the attacker, then uses tcpdump to capture the packets as they pass through</h5>
	</div>
	<div className="w3-card-4 w3-margin">
	  <div className="w3-container w3-dark-grey">
	    <h3>Temperature Sensor</h3>
	    <h6>Python script that reads the temperature using a Sense HAT and posts the data via REST to the server</h6>
	  </div>
	  <div className={'w3-container ' + this.state.sensor.color}>
	    <span className="snort-status"><h3>{this.state.sensor.string}</h3></span>
	    <span className="snort-padding" />
	    <button onClick={this.handleSensor.bind(this)}className="w3-button w3-white w3-border w3-border-black w3-round-large snort-status">{this.state.sensor.button}</button>
	  </div>
	</div>
	<div className="w3-card-4 w3-margin">
	  <div className="w3-container w3-dark-grey">
	    <h3>Arpspoof</h3>
	    <h6>Runs arpspoof twice, once to tell the client that the access points IP address is associated with the attackers MAC address and vice versa.</h6>
	  </div>
	  <div className={'w3-container ' + this.state.arpSpoof.color}>
	    <span className="snort-status"><h3>{this.state.arpSpoof.string}</h3></span>
	    <span className="snort-padding" />
	    <button onClick={this.handleArpSpoof.bind(this)} className="w3-button w3-white w3-border w3-border-black w3-round-large snort-status">{this.state.arpSpoof.button}</button>
	  </div>
	</div>
	<div className="w3-card-4 w3-margin">
	  <div className="w3-container w3-dark-grey">
	    <h3>Tcpdump</h3>
	    <h6>Logs all recorded traffic from the client to a pcap file</h6>
	  </div>
	  <div className={'w3-container ' + this.state.tcpDump.color}>
	    <span className="snort-status"><h3>{this.state.tcpDump.string}</h3></span>
	    <span className="snort-padding" />
	    <button onClick={this.handleTcpDump.bind(this)} className="w3-button w3-white w3-border w3-border-black w3-round-large snort-status">{this.state.tcpDump.button}</button>
	  </div>
	</div>

      </div>
    );
  }
}
