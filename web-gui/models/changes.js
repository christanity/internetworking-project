var mongoose = require('mongoose');

var changeSchema = mongoose.Schema({
  name: String,
  date: Date,
  data: String,
});

module.exports = mongoose.model('Change', changeSchema);
