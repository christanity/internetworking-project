import requests
import time
from sense_hat import SenseHat

# Set ip address and port as needed

address = 'chris-tan.xyz'
port = '80'
url = 'http://{0}:{1}/logtemp'.format(address,port)
user = 'chris'

print(url)

#Retrieve temperature reading from Sense HAT sensor
sense = SenseHat()
def readTemp():
    temperature = sense.get_temperature() - 10
    print(temperature)
    logged_at = time.strftime("%Y/%m/%d %H:%M:%S")
    print(str(logged_at) + "\n")
    payload = {
        'temperature': temperature,
        'logged_at': logged_at,
        'user': user,
    }
    postRequest(payload)

# Time interval to retrieve readings every 5 seconds
def initiate():
    next_time = time.time()
    while True:
        readTemp()
        next_time += 5
        time.sleep(max(0, next_time - time.time()))

#Post generated integers to server
def postRequest(payload):
    try:
        r = requests.post(url, data = payload)
        #print(payload)
    except Exception as e: 
        print(e)

initiate()
